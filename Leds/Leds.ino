String inData = "";
const int noOfLeds = 6;
int led[noOfLeds], value[noOfLeds], fadeVal[noOfLeds];//remove fadeVal
boolean fadeMode = false;

unsigned char maxBrightness = 255;
unsigned char pwmFrequency = 75;

void setup()
{
  //Serial3.begin(9600); //Serial3 on pins 14 and 15
  Serial.begin(9600);
  setLeds();
}

void loop(){
  //if (Serial3.available()) {
  if (Serial.available()) {
    char c = Serial3.read();
    if (c!='\0') inData += c;
    else{
      methods(inData);
      inData = "";
    }
  }
  if (fadeMode) fadeAuto();
}

void setLeds(){
  for (int i=0; i < noOfLeds; i++){
      value[i] = 0;
      led[i] = 10-i;
      pinMode(led[i], OUTPUT);
    }
}

void methods(String in){
  switch (in.charAt(0)){
    case 'o': turnOn(in.substring(1,3).toInt(), in.substring(3,6).toInt());
              if (fadeMode) fadeMode = false;
              break;
    case 'f': turnOff(in.substring(1,3).toInt());
              if (fadeMode) fadeMode = false;
              break;
    case 'd': fade(in.substring(1,3).toInt(), in.substring(3,6).toInt());
              if (fadeMode) fadeMode = false;
              break;
    case 'a': fadeMode = true;
              break;
  }
}

void turnOn(int no, int val){
  //if (val==0) val=200;
  if (no>0 && no<(noOfLeds+1)) fade(no, val);
  else if (no==0) fade(0, val);
}

void turnOff(int no){
  if(no>0 && no<(noOfLeds+1)) fade(no, 0);
  else if (no==0) fade(0, 0);
}

void fade(int no, int newVal){
  if(no>0 && no<(noOfLeds+1)){
    if (newVal > value[no-1] && no<(noOfLeds+1)){
      for (int i=value[no-1]; i < newVal+1; i++){
        analogWrite(led[no-1], i);
        //ShiftPWM.SetOne(led[no-1], i);
        delay(5);
      }  
    } else if (newVal < value[no-1]) {
      for (int i=value[no-1]; i > newVal-1; i--){
        analogWrite(led[no-1], i);
        //ShiftPWM.SetOne(led[no-1], i);
        delay(5);
      }
    }
    value[no-1] = newVal;
  } else if (no == 0) {
    int sign[noOfLeds];
    int maxSteps = 0;
    for (int i = 0; i< noOfLeds; i++){
      if (newVal > value[i]) sign[i] = 1;
      else if (newVal < value[i]) sign[i] = -1;
      //else sign[i] = -1;
      int temp = abs(newVal-value[i]);
      if (temp>maxSteps) maxSteps = temp;
    }
    for (int i=0; i < maxSteps; i++){
      for (int j=0; j < noOfLeds; j++){
        if (value[j]!=newVal){
          value[j] += sign[j];
          analogWrite(led[j], value[j]);    
          //ShiftPWM.SetOne(led[j], value[j]);
        }
      }
      delay(5);
    }
  }
}

void fadeAuto(){
  int change = 0;
  for(int i = 0; i< noOfLeds; i++){
    fadeVal[i] = random(1, 3);
  }
  while(!Serial3.available()){
    /*if (change>200){
      for(int i = 0; i< noOfLeds; i++){
        int changeVal = random(1, 5);
        if(value[i] > 0 && value[i] < 250){
          if (fadeVal[i]<0) fadeVal[i] = -changeVal;
          else fadeVal[i] = changeVal;
        }
      }
      change=0;
    }*/
    for (int i = 0; i< noOfLeds; i++){
      if (value[i] <= 0){
        fadeVal[i] = -fadeVal[i];
        value[i] = 0;
      }
      if (value[i] >= 250){
        fadeVal[i] = -fadeVal[i];
        value[i] = 250;
      }
      value[i] -= fadeVal[i];
      analogWrite(led[i], value[i]);
      //ShiftPWM.SetOne(led[i], value[i]);
    }
    delay(50);
    change++;
  }
}
