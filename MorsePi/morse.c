/*
* morse.c:
* Morse Code LED blink for Raspberry Pi
*/
 
#include "morse.h"
#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

int led = 7;
int M_LONG = 300;
int M_SHORT = 100; 
char* text;

int main (void)
{
	if (wiringPiSetup() == -1) exit (1);
	pinMode(led, OUTPUT);
	
	readData();
	while(strncmp(text, "exit", 6)!=0)  {
		for (int i = 0; text[i] != '\0'; i++) morse(text[i]);
		readData();
	}
}

void blinkLed (long lenghtOfDelay) {
	delay(M_SHORT);
	digitalWrite(led, 1);   // turn the LED on (HIGH is the voltage level)
	delay(lenghtOfDelay);   // wait for a second
	digitalWrite(led, 0);   // turn the LED off by making the voltage LOW
}

void readData () {
	fputs("enter some text: ", stdout);
	fflush(stdout);
	if ( fgets(text, sizeof text, stdin) != NULL )
	{
		char *newline = strchr(text, '\n'); /* search for newline character */
		if ( newline != NULL ) *newline = '\0'; /* overwrite trailing newline */
		//printf("text = \"%s\"\n", text);
	}
}

void morse (char letter) {
	switch (letter) {
		case 'a': case 'A':
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
		break;
      
		case 'b': case 'B':
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
		break;
      
		case 'c': case 'C':
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
		break;
      
		case 'd': case 'D':
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
		break;
      
		case 'e': case 'E':
			blinkLed(M_SHORT);
		break;
    
		case 'f': case 'F':
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
		break;
      
		case 'g': case 'G':
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
		break;
      
		case 'h': case 'H':
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
		break;
      
		case 'i': case 'I':
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
		break;
      
		case 'j': case 'J':
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			blinkLed(M_LONG);
		break;
      
		case 'k': case 'K':
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
		break;
      
		case 'l': case 'L':
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
		break;
      
		case 'm': case 'M':
			blinkLed(M_LONG);
			blinkLed(M_LONG);
		break;
      
		case 'n': case 'N':
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
		break;
      
		case 'o': case 'O':
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			blinkLed(M_LONG);
		break;
      
		case 'p': case 'P':
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
		break;
      
		case 'q': case 'Q':
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
		break;
      
		case 'r': case 'R':
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
		break;
      
		case 's': case 'S':
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
		break;
      
		case 't': case 'T':
			blinkLed(M_LONG);
		break;
      
		case 'u': case 'U':
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
		break;
      
		case 'v': case 'V':
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
		break;
      
		case 'x': case 'X':
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
		break;
      
		case 'y': case 'Y':
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_LONG);
		break;
      
		case 'z': case 'Z':
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
		break;
		
		case '.':
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
		
		case ',':
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			blinkLed(M_SHORT);
			blinkLed(M_SHORT);
			blinkLed(M_LONG);
			blinkLed(M_LONG);
			
		break;
      
		default:
			delay(M_LONG);
		break;
	} 
	delay(M_LONG);
}
