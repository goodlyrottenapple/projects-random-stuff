import processing.serial.*;
import java.lang.Exception.*;
import java.awt.event.*;
import controlP5.*;

ControlP5 controlP5;
DropdownList ports;
Serial myPort;  // Create object from Serial class
int selectedPort;
String[] comList;
String morseTree = "*5*H*4*S***V*3*I***F***U?*_**2*E***L***R*+.****A***P@**W***J*1* *6-B*=*D***X***N***C;*!K*()Y***T*7*Z**,G***Q***M:8*!***O*9***0*";
char letter = '/';
int pointer = 63, jumper = 32;
boolean looping = false;

void setup() 
{
  size(800, 600);
  if (frame != null) {
    frame.setResizable(true);
  }
  
  frame.addComponentListener(new ComponentAdapter() {
    public void componentResized(ComponentEvent e) {
      if(e.getSource()==frame) {
        redraw();
      }
    }
  });
  
  controlP5 = new ControlP5(this);
  //Make a dropdown list calle ports. Lets explain the values: ("name", left margin, top margin, width, height (84 here since the boxes have a height of 20, and theres 1 px between each item so 4 items (or scroll bar).
  ports = controlP5.addDropdownList("ports-list",30,25,200,184).hide();
  customize(ports);
  
  noLoop();
}

void customize (DropdownList d){
  /*d.setBackgroundColor(color(200));
  //Set the height of each item when the list is opened.
  d.setItemHeight(20);
  //Set the height of the bar itself.
  //Set the lable of the bar when nothing is selected.
  d.captionLabel().set("Select COM port");
  //Set the top margin of the lable.
  */
  d.setBarHeight(15);
  d.captionLabel().style().marginTop = 3;
  d.captionLabel().style().marginLeft = 3;
  d.valueLabel().style().marginTop = 3;
  
  comList = myPort.list();
  for(int i=0; i< comList.length; i++)
  {
    //This is the line doing the actual adding of items, we use the current loop we are in to determin what place in the char array to access and what item number to add it as.
    d.addItem(comList[i],i);
  }
  
}

void controlEvent(ControlEvent theEvent) {
  if (theEvent.isGroup()) 
  {
    //Store the value of which box was selected, we will use this to acces a string (char array).
    float S = theEvent.group().value();
    //Since the list returns a float, we need to convert it to an int. For that we us the int() function.
    selectedPort = int(S);
    println(comList[selectedPort]);
    startSerial();
  }
}

void mouseReleased() {
  if (mouseX > 5 && mouseX < 29 && 
      mouseY > 4 && mouseY < 28) {
        if(!looping){
          loop();
          looping = true;
          ports.show();
        } else {
          looping = false;
          fill(255);
          rect(10, 9, 14, 14);
          noLoop();
          ports.hide();
        }
      }
}

void draw()
{
  background(190);
  if (!looping) fill(255);
  else fill(0);
  rect(10, 9, 14, 14);
  drawTree();
  highlightTree(letter);
}

void startSerial()
{
  try{
    //When this function is called, we setup the Serial connection with the accuried values. The int Ss acesses the determins where to accsess the char array. 
    myPort = new Serial(this, comList[selectedPort], 9600);
    myPort.bufferUntil(0);
  } catch (RuntimeException e) {
    if (e.getMessage().contains("<init>"))
      println("error...port " + comList[selectedPort] + " is out of cheese");
  }
  looping = false;
  fill(255);
  rect(10, 9, 14, 14);
  noLoop();
  //Since this is a one time setup, we state that we now have set up the connection.
}

void serialEvent(Serial p) { 
  String in = p.readString(); 
  System.out.println(in);
  if(in.equals("SHORT\0"))
  {
    pointer -= jumper;
    jumper /= 2;
    letter = morseTree.charAt(pointer);
    redraw();
  }
  else if(in.equals("LONG\0"))
  {
    pointer += jumper;
    jumper /= 2;
    letter = morseTree.charAt(pointer);
    redraw();
  }
  else if(in.equals("PAUSE\0"))
  {
    pointer = 63;
    jumper = 32;
    letter = '%';
    redraw();
  }
}

void highlightTree(char letter){
  int yDif = height/8;
  int temp = (int)(width*0.9);
  smooth(4);
  textAlign(CENTER, CENTER);
  
  switch(letter){
    case 'E':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      break;
    case 'T':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      break;
    case 'I':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      break;
    case 'A':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      break;
    case 'N':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      break;
    case 'M':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      break;
    case 'S':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      break;
    case 'U':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("U", (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      break;
    case 'R':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("R", (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
      break;
    case 'W':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("W", (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      break;
    case 'D':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("D", (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      break;
    case 'K':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("K", (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
      break;
    case 'G':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("G", (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      break;
    case 'O':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("O", (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      break;
    case 'H':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("H", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      break;
    case 'V':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("V", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      break;
    case 'F':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("U", (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
      text("F", (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
      break;
    case 'Ü':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("U", (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("Ü", (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
      break;
    case 'L':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("R", (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("L", (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
      break;
    case 'Ä':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("R", (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);      
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("Ä", (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
      break;
    case 'P':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("W", (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
      text("P", (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
      break;
    case 'J':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("W", (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("J", (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      break;
    case 'B':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("D", (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("B", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      break;
    case 'X':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("D", (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("X", (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      break;
    case 'C':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("K", (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
      text("C", (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
      break;
    case 'Y':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("K", (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("Y", (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
      break;
    case 'Z':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("G", (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("Z", (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
      break;
    case 'Q':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("G", (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("Q", (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
      break;
    case 'Ö':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("O", (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
      text("Ö", (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
      break;
    /*case 'CH':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("O", (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("CH", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      break;*/
    case '5':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("H", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
      text("5", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
      break;
    case '4':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("H", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
      text("4", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
      break;
    case 'Š':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("V", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
      text("Š", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
      break;
    case '3':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("V", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
      text("3", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
      break;
     case 'É':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("U", (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
      text("F", (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
      text("É", (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
      break;
    case '2':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
      text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      text("U", (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("Ü", (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
      text("2", (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
      break;
    case '+':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("R", (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);      
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("Ä", (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
      text("+", (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
      break;
     case '1':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
      patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)-(temp/4), yDif*2);
      text("E", (width/2)-(temp/4), yDif*2);
      hiCircle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
      text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("W", (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("J", (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
      text("1", (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
      break;
    case '6':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("D", (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("B", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
      text("6", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
      break;
    case '=':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("D", (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("B", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
      text("=", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
      break;
    case '/':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
      text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      text("D", (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      text("X", (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
      text("/", (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
      break;
    case '7':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
      patternLine((width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      patternLine((width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      text("G", (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
      text("Z", (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
      text("7", (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
      break;
    case '8':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("O", (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
      text("Ö", (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
      text("8", (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
      break;
    case '9':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("O", (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("CH", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
      text("9", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
      break;
    case '0':
      stroke(204, 102, 0);
      patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
      patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
      patternLine((width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
      stroke(0);
      hiCircle(80, width/2, yDif-yDif/3);
      text("START", width/2, yDif-yDif/3);
      hiCircle(20, (width/2)+(temp/4), yDif*2);
      text("T",(width/2)+(temp/4), yDif*2);
      hiCircle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
      text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      text("O", (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      text("CH", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
      hiCircle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
      text("0", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
      break;
  }
}

void drawTree()
{
  int yDif = height/8;
  int temp = (int)(width*0.9);
  float[] dot = {.01, 5};
  float[] dash = {5, 5};
  smooth(4);
  textAlign(CENTER, CENTER);
  
  patternLine(width/2, yDif-yDif/3, (width/2)-(temp/4), yDif*2, 0x5555, 3);
  patternLine(width/2, yDif-yDif/3, (width/2)+(temp/4), yDif*2, 0x5555, 15);
  
  circle(80, width/2, yDif-yDif/3);
  text("START", width/2, yDif-yDif/3);
  
  patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)-(temp/8), yDif*3, 0x5555, 3);
  patternLine((width/2)-(temp/4), yDif*2, (width/2)-(temp/4)+(temp/8), yDif*3, 0x5555, 15);
  patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)-(temp/8), yDif*3, 0x5555, 3);
  patternLine((width/2)+(temp/4), yDif*2, (width/2)+(temp/4)+(temp/8), yDif*3, 0x5555, 15);
  
  circle(20, (width/2)-(temp/4), yDif*2);
  circle(20, (width/2)+(temp/4), yDif*2);
  
  text("E", (width/2)-(temp/4), yDif*2);
  text("T",(width/2)+(temp/4), yDif*2);
  
  patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
  patternLine((width/2)-(temp/4)-(temp/8), yDif*3, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
  patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
  patternLine((width/2)-(temp/4)+(temp/8), yDif*3, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
  patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, 0x5555, 3);
  patternLine((width/2)+(temp/4)-(temp/8), yDif*3, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, 0x5555, 15);
  patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, 0x5555, 3);
  patternLine((width/2)+(temp/4)+(temp/8), yDif*3, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, 0x5555, 15);
  
  circle(20, (width/2)-(temp/4)-(temp/8), yDif*3);
  circle(20, (width/2)-(temp/4)+(temp/8), yDif*3);
  circle(20, (width/2)+(temp/4)-(temp/8), yDif*3);
  circle(20, (width/2)+(temp/4)+(temp/8), yDif*3);
  
  text("I", (width/2)-(temp/4)-(temp/8), yDif*3);
  text("A", (width/2)-(temp/4)+(temp/8), yDif*3);
  text("N", (width/2)+(temp/4)-(temp/8), yDif*3);
  text("M", (width/2)+(temp/4)+(temp/8), yDif*3);
  
  patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  patternLine((width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  patternLine((width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  patternLine((width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  patternLine((width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  patternLine((width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  patternLine((width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  patternLine((width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, 0x5555, 3);
  patternLine((width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, 0x5555, 15);
  
  circle(20, (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
  circle(20, (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
  circle(20, (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
  circle(20, (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
  circle(20, (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
  circle(20, (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
  circle(20, (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
  circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
  
  text("S", (width/2)-(temp/4)-(temp/8)-(temp/16), yDif*4);
  text("U", (width/2)-(temp/4)-(temp/8)+(temp/16), yDif*4);
  text("R", (width/2)-(temp/4)+(temp/8)-(temp/16), yDif*4);
  text("W", (width/2)-(temp/4)+(temp/8)+(temp/16), yDif*4);
  text("D", (width/2)+(temp/4)-(temp/8)-(temp/16), yDif*4);
  text("K", (width/2)+(temp/4)-(temp/8)+(temp/16), yDif*4);
  text("G", (width/2)+(temp/4)+(temp/8)-(temp/16), yDif*4);
  text("O", (width/2)+(temp/4)+(temp/8)+(temp/16), yDif*4);
  
  patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  patternLine((width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  patternLine((width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  //patternLine((width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  patternLine((width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  //patternLine((width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  patternLine((width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  //patternLine((width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  //patternLine((width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  patternLine((width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  
  patternLine((width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  patternLine((width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  patternLine((width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  //patternLine((width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  //patternLine((width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  patternLine((width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  //patternLine((width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  patternLine((width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  //patternLine((width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  patternLine((width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6, 0x5555, 3);
  patternLine((width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6, 0x5555, 15);
  
  
  circle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
  circle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
  circle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
  circle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
  circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
  
  text("H", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
  text("V", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
  text("F", (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
  text("Ü", (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
  text("L", (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
  text("Ä", (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
  text("P", (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
  text("J", (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
  text("B", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32), yDif*5);
  text("X", (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32), yDif*5);
  text("C", (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32), yDif*5);
  text("Y", (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32), yDif*5);
  text("Z", (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32), yDif*5);
  text("Q", (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32), yDif*5);
  text("Ö", (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32), yDif*5);
  text("CH", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32), yDif*5);
  
  circle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  circle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  circle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  circle(20, (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  circle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  circle(20, (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  circle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  //circle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  circle(20, (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6); 
  circle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  circle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  circle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
  circle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  //circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  circle(20, (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
  
  text("5", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  text("4", (width/2)-(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  text("Š", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  text("3", (width/2)-(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  text("É", (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)-(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  text("2", (width/2)-(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)+(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  text("+", (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)+(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)+(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  text(" ", (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  text("1", (width/2)-(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6); 
  text("6", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  text("=", (width/2)+(temp/4)-(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  text("/", (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)-(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)-(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)-(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
  text("7", (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)+(temp/8)-(temp/16)-(temp/32)+(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)+(temp/8)-(temp/16)+(temp/32)+(temp/64), yDif*6);
  text("8", (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)-(temp/64), yDif*6);
  text(" ", (width/2)+(temp/4)+(temp/8)+(temp/16)-(temp/32)+(temp/64), yDif*6);
  text("9", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)-(temp/64), yDif*6);
  text("0", (width/2)+(temp/4)+(temp/8)+(temp/16)+(temp/32)+(temp/64), yDif*6);
}

void circle(int size, int x, int y)
{
  ellipseMode(CENTER);
  fill(255);
  ellipse(x,y, size, size);
  fill(0);
}

void hiCircle(int size, int x, int y)
{
  ellipseMode(CENTER);
  fill(204, 102, 0);
  ellipse(x,y, size, size);
  fill(0);
}

void patternLine(int xStart, int yStart, int xEnd, int yEnd, int linePattern, int lineScale) {
  int temp, yStep, x, y;
  int pattern = linePattern;
  int carry;
  int count = lineScale;
  
  boolean steep = (abs(yEnd - yStart) > abs(xEnd - xStart));
  if (steep == true) {
    temp = xStart;
    xStart = yStart;
    yStart = temp;
    temp = xEnd;
    xEnd = yEnd;
    yEnd = temp;
  }    
  if (xStart > xEnd) {
    temp = xStart;
    xStart = xEnd;
    xEnd = temp;
    temp = yStart;
    yStart = yEnd;
    yEnd = temp;
  }
  int deltaX = xEnd - xStart;
  int deltaY = abs(yEnd - yStart);
  int error = - (deltaX + 1) / 2;
  
  y = yStart;
  if (yStart < yEnd) {
    yStep = 1;
  } else {
    yStep = -1;
  }
  for (x = xStart; x <= xEnd; x++) {
    if ((pattern & 1) == 1) {
  if (steep == true) {
    point(y, x);
  } else {
    point(x, y);
  }
  carry = 0x8000;
    } else {
  carry = 0;
    }  
    count--;
    if (count <= 0) {
  pattern = (pattern >> 1) + carry;
  count = lineScale;
    }
    
    error += deltaY;
    if (error >= 0) {
  y += yStep;
  error -= deltaX;
    }
  }
}

  
  
  
  
  //myPort.write('c');
  //myPort.write('x');
