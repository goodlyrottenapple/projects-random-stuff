#include "Arduino.h"
void setup();
void loop();
/*
  Morse Code LED blink
 */
 
// Pin 13 has an LED connected on most Arduino boards.

int led = 13;
int M_LONG = 300;
int M_SHORT = 100;

void setup() {                
  // initialize the digital pin as an output.
  Serial.begin(9600); //Serial3 on pins 14 and 15
  pinMode(led, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() {
  if (Serial.available()) {
    char ser = Serial.read();
    morse(ser);
  }
}

void blinkLed (long lenghtOfDelay) {
  delay(M_SHORT);
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(lenghtOfDelay);      // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
}

void morse (char letter) {
  switch (letter) {
    case 'a': case 'A':
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      break;
      
    case 'b': case 'B':
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      break;
      
    case 'c': case 'C':
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      break;
      
    case 'd': case 'D':
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      break;
      
    case 'e': case 'E':
      blinkLed(M_SHORT);
      break;
    
    case 'f': case 'F':
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      break;
      
    case 'g': case 'G':
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      break;
      
    case 'h': case 'H':
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      break;
      
    case 'i': case 'I':
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      break;
      
    case 'j': case 'J':
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      break;
      
    case 'k': case 'K':
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      break;
      
    case 'l': case 'L':
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      break;
      
    case 'm': case 'M':
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      break;
      
    case 'n': case 'N':
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      break;
      
    case 'o': case 'O':
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      break;
      
    case 'p': case 'P':
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      break;
      
    case 'q': case 'Q':
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      break;
      
    case 'r': case 'R':
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      break;
      
    case 's': case 'S':
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      break;
      
    case 't': case 'T':
      blinkLed(M_LONG);
      break;
      
    case 'u': case 'U':
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      break;
      
    case 'v': case 'V':
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      break;
      
    case 'x': case 'X':
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      break;
      
    case 'y': case 'Y':
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      break;
      
    case 'z': case 'Z':
      blinkLed(M_LONG);
      blinkLed(M_LONG);
      blinkLed(M_SHORT);
      blinkLed(M_SHORT);
      break;
      
    default:
      delay(M_LONG);
      break;
  } 
  delay(M_LONG);
}
