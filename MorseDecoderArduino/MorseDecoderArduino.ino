int sensorValue, lastButtonState = HIGH, reading = LOW, pointer = 63, jumper = 32;
long lastDebounceTime = 0, debounceDelay=98;
boolean isReading = false;
String morseTree = "*5*H*4*S***V*3*I***F***U?*_**2*E***L***R*+.****A***P@**W***J*1* *6-B*=*D***X***N***C;*!K*()Y***T*7*Z**,G***Q***M:8*!***O*9***0*", text = "", inData ="";

void setup() {
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if (!isReading && Serial.available()) {
    char c = Serial.read();
    if (c!='x') inData += c; //'\0'
    else{
      switch (inData.charAt(0)){
        case 'c': text="";
                  Serial.println("clear");
                  break;
        case 's': debounceDelay = inData.substring(1,4).toInt();
                  break;
      }
      inData = "";
    }
  }
  sensorValue = analogRead(A0);
  if(sensorValue > 600){
    reading = HIGH;
    if(!isReading){
      isReading = true;
      lastDebounceTime = millis();
    }
  }
  else reading = LOW;
  digitalWrite(13, reading);
  
  if((millis() - lastDebounceTime) > debounceDelay*6 && isReading){
    text += morseTree.charAt(pointer);
    text += " ";
    //Serial.println(text);
    pointer = 63;
    jumper = 32;
    Serial.println("<" + text + ">");
    isReading = false;
  }
  
  if (reading != lastButtonState) {
    if((millis() - lastDebounceTime) > debounceDelay*3){
      if(lastButtonState == HIGH){
        //Serial.println("LONG");
        Serial.write("LONG");
        Serial.write(0);
        pointer += jumper;
        jumper /= 2;
      }else{
        text += morseTree.charAt(pointer);
        pointer = 63;
        jumper = 32;
        //Serial.println("<" + text + ">");
      }
    }
    else if((millis() - lastDebounceTime) > debounceDelay && lastButtonState == HIGH){
      //Serial.println("SHORT");
      Serial.write("SHORT");
      Serial.write(0);
      pointer -= jumper;
      jumper /= 2;
    }
    lastDebounceTime = millis();
    lastButtonState = reading;
  }
  delay(1);
}
